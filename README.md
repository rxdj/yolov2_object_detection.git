# yolov2_object_detection

#### 介绍
IOS, coreML, yolov2 object detection

coreML objective-C 接口,yolov2 实现物体检测。

原swift版本在这里：https://github.com/syshen/YOLO-CoreML 

mlmodel太大，无法上传，请参考上面github仓库里的readMe指定方法下载。下载后修改名字为yoloModel，添加到工程即可。
