1.简介：
	yolov2model前向预测实现场景中的物体检测，能识别80类物体

2. 依赖库：
	opencv2.framework

3. 接口:
    * int CDetectObject::init(const BOOL useCpuOnly, const MLComputeUnits computeUnit, const std::string& classtxtPath, const cv::Size& scaleSize)
    功能：
    	类初始化
    输入：
    	useCpuOnly ----计算过程是否仅适用CPU
    	computeUnit ----计算单元设置
    	classtxtPath ----80个类的类标签文件所在路径
    	scaleSize ----图像预处理中设置的尺寸大小

    输出：
    	无
    返回值：
    	0 ----success
    	-1 ----fail  未能加载类标签文件


   	* int CDetectObject::implDetection(const cv::Mat& image, std::vector<DetectionInfo>& detectionResults)
   	功能：
   		执行前向预测，并获取检测结果
   	输入：
   		image ----需要检测的图像
   	输出：
   		detectionResults ----检测得到的结果
   	返回值：
   		0 ----success
   		-1 ----fail  未能加载图像数据

