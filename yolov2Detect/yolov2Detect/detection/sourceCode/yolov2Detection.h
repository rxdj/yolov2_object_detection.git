//
//  yolov2Detection.h
//  yolov2Detect
//
//  Created by riddick on 2019/3/30.
//  Copyright © 2019 riddick. All rights reserved.
//

#ifndef yolov2Detection_h
#define yolov2Detection_h
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <iostream>
#include <fstream>
#include <CoreML/CoreML.h>
#include <CoreML/MLExport.h>
#include "yoloModel.h"
#include "pixelBufferPool.h"

struct DetectionInfo {
    std::string name;
    float confidence;
    cv::Rect2d box;
};


class CDetectObject{
    
public:
    CDetectObject();
    ~CDetectObject();
    
private:
    yoloModel *Model;
    MLPredictionOptions* option;
    MLModelConfiguration* config;
    cv::Size inputSize;
    std::vector<std::string> classes;
    
    int maxBoundingBoxes;
    float confidenceThreshold;
    float nmsThreshold;
    std::vector<float> anchors;
    
private:
    float sigmoid(float x);
    void mySoftMax(std::vector<float>& weightList);
    void softmax( std::vector<float>& inputOutputArray);
    int loadClasstxt(const std::string& classtxt, std::vector<std::string>& classes);
    MLMultiArray* predictImageScene(const cv::Mat& imgTensor);
    void preprocessImage(const cv::Mat& image, cv::Mat& inputImage);
   // void parseFeature(MLMultiArray* feature, std::vector<DetectionInfo>& detections);
    void parseFeature(MLMultiArray* feature, std::vector<int>& ids, std::vector<float>& confidences, std::vector<cv::Rect>& boxes);
    
public:
    int init(const bool useCpuOnly,
             const MLComputeUnits computeUnit,
             const std::string& classtxtPath,
             const cv::Size& scaleSize);
    
    int implDetection(const cv::Mat& image, std::vector<DetectionInfo>& detectionResults);
    
};


#endif /* yolov2Detection_h */
