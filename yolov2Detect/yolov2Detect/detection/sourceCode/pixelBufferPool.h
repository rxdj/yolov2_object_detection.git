
#pragma once
#import <Foundation/Foundation.h>
#include <CoreVideo/CVPixelBuffer.h>
#include <CoreVideo/CVPixelBufferPool.h>
#import <opencv2/highgui/highgui.hpp>
#import <opencv2/core/core.hpp>
#import <opencv2/imgproc/imgproc.hpp>
#import <opencv2/videoio/videoio.hpp>
#import <opencv2/imgcodecs/imgcodecs.hpp>
    
class PixelBufferPool{
public:
    PixelBufferPool();
    ~PixelBufferPool();
    CVPixelBufferRef GetPixelBuffer(const cv::Mat &mat);
    static std::shared_ptr<cv::Mat> GetMat(CVPixelBufferRef pixelBuffer);
    void SavePixelBufferToiPhoneAlbum(CVPixelBufferRef pixelBuffer);
    void SaveMatToiPhoneAlbum(const cv::Mat &mat);
    void SaveMatToFile(const cv::Mat &mat, NSString *filePath);
protected:
    CVPixelBufferPoolRef CreateCVPixelBufferPool(int width, int height);
    void ReleaseCVPixelBufferPool(CVPixelBufferPoolRef pool);
protected:
    CVPixelBufferPoolRef pool_;
};
